# rav1e's Website

This is the website for rav1e project. Live at [https://mindfreeze.videolan.me/rav1e](https://mindfreeze.videolan.me/rav1e)


## Getting Started

Building the website depends on [Hugo](http://gohugo.io). So, first make sure
that you have it installed. If on OS X and using Homebrew, run the following:

```sh
brew update && brew install hugo
```

Then, get the website running locally:

```sh
git clone http://code.videolan.org/mindfreeze/rav1e
cd website
hugo server
```

Then visit [http://localhost:1313](http://localhost:1313).

## License

This project is licensed under the [MIT license](LICENSE).

### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in Tokio by you, shall be licensed as MIT, without any additional
terms or conditions.

### Development

The Website is build upon [Tokio Website](https://tokio.rs/).
